/* scrollUp Minimum setup */
$(function () {
    $.scrollUp();
});
$(document).ready(function () {

    "use strict";
    /*home slider using owl carousel*/
    $(".home_slider_wrapper").owlCarousel({
        items: 1,
        dots: false,
        autoplay: true,
        autoplayTimeout: 3000,
        loop: true,
        animateOut: 'fadeOut',
        nav: false,
        mouseDrag: false,
        touchDrag: false
    });
    /*$(".home_slider_wrapper").on("translate.owl.carousel", function(){
            $(".single_slider h2").removeClass("animated fadeInUp").css("opacity", "0");
            $(".single_slider h1").removeClass("animated fadeInUp").css("opacity", "0");
            $(".single_slider p").removeClass("animated fadeInUp").css("opacity", "0");
            $(".single_slider a").removeClass("animated fadeInDown").css("opacity", "0");
        });
        
        $(".home_slider_wrapper").on("translated.owl.carousel", function(){
            $(".single_slider h2").addClass("animated fadeInUp").css("opacity", "1");
            $(".single_slider h1").addClass("animated fadeInUp").css("opacity", "1");
            $(".single_slider p").addClass("animated fadeInUp").css("opacity", "1");
            $(".single_slider a").addClass("animated fadeInDown").css("opacity", "1");
        });*/
    new WOW().init();

    $(".video").click(function () {
        $.fancybox({
            'autoScale': false,
            'transitionIn': 'none',
            'transitionOut': 'none',
            'title': this.title,
            'width': 640,
            'height': 385,
            'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
            'type': 'swf',
            'swf': {
                'wmode': 'transparent',
                'allowfullscreen': 'true'
            }
        });

        return false;
    });


    /*-------------single service carousel------------*/
    $(".service_wrapper").owlCarousel({
        items: 1,
        dots: true,
        autoplay: true,
        smartSpeed: 1000,
        loop: true,
        nav: false

    });
    
    /*----------single service carousel--------------*/
    $(".blog_carousel_wrapper").owlCarousel({
        items: 3,
        dots: false,
        autoplay: false,
        smartSpeed: 1000,
        loop: true,
        nav: true,
        margin:30

    });


});